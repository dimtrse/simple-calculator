package com.ds;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Main
 *
 * @version 1.0
 */
public class Main {
    BufferedReader bufferedReader;

    public Main(InputStreamReader isr) {
        bufferedReader = new BufferedReader(isr);
    }

    public void run() {

        Calculator calculator = new Calculator();
        int state = 0;
        String inputString;

        try {
            while (true) {
                switch (state) {
                    case 0:
                        System.out.print("Operand #1: ");
                        inputString = bufferedReader.readLine();
                        if (inputString.isEmpty())
                            break;
                        calculator.setOperand1(Double.parseDouble(inputString));

                        state = 1;
                        break;
                    case 1:
                        System.out.print("Operand #2: ");
                        inputString = bufferedReader.readLine();
                        if (inputString.isEmpty()) {
                            break;
                        }
                        calculator.setOperand2(Double.parseDouble(inputString));

                        state = 2;
                        break;
                    case 2:
                        System.out.print("Operation(+, -, *, /): ");
                        inputString = bufferedReader.readLine();
                        if (inputString.isEmpty())
                            break;

                        calculator.setOperation(inputString.charAt(0));
                        calculator.run();

                        System.out.println("Result: " + calculator.getResult());

                        state = 3;
                    case 3:
                        System.out.print("1. Continue with this result, 2. Input new operands, 3. Exit: ");
                        inputString = bufferedReader.readLine();
                        if (inputString.isEmpty())
                            break;
                        int next = Integer.parseInt(inputString);

                        switch (next) {
                            case 1:
                                calculator.setOperand1(calculator.getResult());
                                System.out.println("Operand 1: " + calculator.getOperand1());

                                state = 1;
                                break;
                            case 2:
                            default:
                                state = 0;
                                break;
                            case 3:
                                System.out.println("Exit from the program");
                                return;

                        }
                    default:
                        break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new Main(new InputStreamReader(System.in)).run();
    }
}