package com.ds;

public class Calculator {
    public double operand1 = 0;
    public double operand2 = 0;
    public char operation = '+';
    public double result = 0;

    public Calculator(double operand1, double operand2, char operation) {
        this.operand1 = operand1;
        this.operand2 = operand2;
        this.operation = operation;
    }

    public Calculator(double operand1, double operand2) {
        this.operand1 = operand1;
        this.operand2 = operand2;
    }

    public Calculator() {
    }

    public double add() {
        return operand1 + operand2;
    }

    public double minus() {
        return operand1 - operand2;
    }

    public double multiply() {
        return operand1 * operand2;
    }

    public double divide() {
        if (operand2 == 0)
            throw new IllegalArgumentException("Cannot divide by zero!");
        return operand1 / operand2;
    }

    public void run() {
        switch (this.operation) {
            case '+':
                this.result = add();
                break;
            case '-':
                this.result = minus();
                break;
            case '*':
                this.result = multiply();
                break;
            case '/':
                this.result = divide();
                break;
        }
    }

    public void clear() {
        this.operand1 = 0;
        this.operand2 = 0;
        this.operation = '+';
        this.result = 0;
    }

    public double getOperand1() {
        return operand1;
    }

    public void setOperand1(double operand1) {
        this.operand1 = operand1;
    }

    public double getOperand2() {
        return operand2;
    }

    public void setOperand2(double operand2) {
        this.operand2 = operand2;
    }

    public char getOperation() {
        return operation;
    }

    public void setOperation(char operation) {
        this.operation = operation;
    }

    public double getResult() {
        return result;
    }

    @Override
    public String toString() {
        return "Calculator{" +
                "operand1=" + operand1 +
                ", operand2=" + operand2 +
                ", operation=" + operation +
                ", result=" + result +
                '}';
    }
}
