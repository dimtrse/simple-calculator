package com.ds;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CalculatorTest {

    @Test
    public void add() {
        Calculator calculator = new Calculator(2, 4, '+');
        calculator.run();

        Assert.assertEquals(calculator.getResult(), 6, 0);


        calculator = new Calculator(15215.124, 5252.555, '+');
        calculator.run();

        Assert.assertEquals(calculator.getResult(), 20467.679, 0.0001);

        calculator = new Calculator(0, 0, '+');
        calculator.run();

        Assert.assertEquals(calculator.getResult(), 0, 0);
    }

    @Test
    public void minus() {
        Calculator calculator = new Calculator(10, 15, '-');
        calculator.run();

        Assert.assertEquals(calculator.getResult(), -5, 0);


        calculator = new Calculator(15215.100, 5252.555, '-');
        calculator.run();

        Assert.assertEquals(calculator.getResult(), 9962.545, 0.0001);

        calculator = new Calculator(0, 0, '-');
        calculator.run();

        Assert.assertEquals(calculator.getResult(), 0, 0);
    }

    @Test
    public void multiply() {
        Calculator calculator = new Calculator(2, 5, '*');
        calculator.run();

        Assert.assertEquals(calculator.getResult(), 10, 0);

        calculator = new Calculator(152555.100, 5252.555, '*');
        calculator.run();

        Assert.assertEquals(calculator.getResult(), 8.013040532805E8, 0.0001);

        calculator = new Calculator(0, 0, '*');
        calculator.run();

        Assert.assertEquals(calculator.getResult(), 0, 0);
    }

    @Test
    public void divide() {
        Calculator calculator = new Calculator(10, 2, '/');
        calculator.run();

        Assert.assertEquals(calculator.getResult(), 5, 0);

        calculator = new Calculator(55225.147, 5252.555, '/');
        calculator.run();

        Assert.assertEquals(calculator.getResult(), 10.513958825752418, 0.0001);

        calculator = new Calculator(0, 0, '/');
        Assertions.assertThrows(IllegalArgumentException.class, calculator::run);
    }

    @Test
    void clear() {
        Calculator calculator = new Calculator(10, 2, '/');
        calculator.run();

        calculator.clear();

        Assert.assertEquals('+', calculator.getOperation());
        Assert.assertEquals(0, calculator.getOperand1(), 0);
        Assert.assertEquals(0, calculator.getOperand2(), 0);
        Assert.assertEquals(0, calculator.getResult(), 0);
    }

    @Test
    void getOperand1() {
        Calculator calculator = new Calculator(10, 2, '/');
        Assert.assertEquals(calculator.getOperand1(), 10, 0);

        calculator = new Calculator();
        Assert.assertEquals(calculator.getOperand1(), 0, 0);
    }

    @Test
    void setOperand1() {
        Calculator calculator = new Calculator();
        calculator.setOperand1(12);
        Assert.assertEquals(calculator.getOperand1(), 12, 0);

        calculator = new Calculator(1, 5, '+');
        calculator.setOperand1(14);
        Assert.assertEquals(calculator.getOperand1(), 14, 0);

    }

    @Test
    void getOperand2() {
        Calculator calculator = new Calculator(10, 2, '/');
        Assert.assertEquals(calculator.getOperand2(), 2, 0);

        calculator = new Calculator();
        Assert.assertEquals(calculator.getOperand2(), 0, 0);
    }

    @Test
    void setOperand2() {
        Calculator calculator = new Calculator();
        calculator.setOperand2(6);
        Assert.assertEquals(calculator.getOperand2(), 6, 0);

        calculator = new Calculator(1, 5, '+');
        calculator.setOperand2(2);
        Assert.assertEquals(calculator.getOperand2(), 2, 0);
    }

    @Test
    void getOperation() {
        Calculator calculator = new Calculator();
        Assert.assertEquals(calculator.getOperation(), '+');

        calculator = new Calculator(2, 3, '-');
        Assert.assertEquals(calculator.getOperation(), '-');

        calculator.setOperation('/');
        Assert.assertEquals(calculator.getOperation(), '/');
    }

    @Test
    void setOperation() {
        Calculator calculator = new Calculator();
        calculator.setOperation('*');
        Assert.assertEquals(calculator.getOperation(), '*');

        calculator = new Calculator(2, 4, '-');
        calculator.setOperation('/');
        Assert.assertEquals(calculator.getOperation(), '/');
    }

    @Test
    void getResult() {
        Calculator calculator = new Calculator(12, 8, '*');
        calculator.run();

        Assert.assertEquals(calculator.getResult(), 96, 0);

        calculator = new Calculator();
        Assert.assertEquals(calculator.getResult(), 0, 0);
    }
}